package br.edu.fatecsjc;

public interface OperacoesAritmeticas {

    int calcularPotenciaEntreDoisNumerosInteiros();

    int calcularSomaEntreDoisNumerosInteiros();

    int calcularSubracaoEntreDoisNumerosInteiros();

    int calcularMultiplicacaoEntreDoisNumerosInteiros();

    int calcularDivisaoEntreDoisNumumerosInteiros();

    int calcularModuloEntreDoisNumerosInteiros();
}
