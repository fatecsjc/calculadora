package br.edu.fatecsjc;

public class Calculadora implements OperacoesAritmeticas {

    private int n1;
    private int n2;

    public Calculadora() {
    }

    public Calculadora(int n1, int n2) {
        this.n1 = n1;
        this.n2 = n2;
    }

    public int getN1() {
        return n1;
    }

    public void setN1(int n1) {
        this.n1 = n1;
    }

    public int getN2() {
        return n2;
    }

    public void setN2(int n2) {
        this.n2 = n2;
    }


    @Override
    public int calcularPotenciaEntreDoisNumerosInteiros() {

        return (int) Math.pow(getN1(), getN2());
    }

    @Override
    public int calcularSomaEntreDoisNumerosInteiros() {

        return getN1() + getN2();
    }

    @Override
    public int calcularSubracaoEntreDoisNumerosInteiros() {

        return getN1() - getN2();
    }

    @Override
    public int calcularMultiplicacaoEntreDoisNumerosInteiros() {

        return getN1() * getN2();
    }

    @Override
    public int calcularDivisaoEntreDoisNumumerosInteiros() {
        int result = 0;
        try {
            result = getN1() / getN2();
        } catch (ArithmeticException e) {
            e.getMessage();
        }

        return result;
    }

    @Override
    public int calcularModuloEntreDoisNumerosInteiros() {

        int result = 0;

        try {
            result = getN1() % getN2();
        } catch (ArithmeticException e) {
            e.getMessage();
        }

        return result;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("N1: ").append(getN1()).append("\tN2: ").append(getN2())
                .append("\nPotencia: ").append(calcularPotenciaEntreDoisNumerosInteiros())
                .append("\nSoma: ").append(calcularSomaEntreDoisNumerosInteiros())
                .append("\nSubtra��o: ").append(calcularSubracaoEntreDoisNumerosInteiros())
                .append("\nProduto: ").append(calcularMultiplicacaoEntreDoisNumerosInteiros())
                .append("\nDivis�o: ").append(calcularDivisaoEntreDoisNumumerosInteiros())
                .append("\nM�dulo: ").append(calcularModuloEntreDoisNumerosInteiros());

        return sb.toString();
    }
}
